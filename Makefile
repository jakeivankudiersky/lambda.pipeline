PROJECT_NAME := <name of project> #required
PKG := gitlab.com/<Gitlab Account>/$(PROJECT_NAME)  #required to set
FUNCTION_NAME := $(PROJECT_NAME)
REGION := <region> #required to set 

dep: ## Get the dependencies
	@go get -v -d ./...

test: dep  ## Run unit tests
	@mkdir -p artifacts
	@cd cmd/$(PROJECT_NAME) && go test ./... -race -cover -coverprofile=../../code-coverage.out

coverage: test ## Generate global code coverage report
	@cd cmd/$(PROJECT_NAME) && go tool cover -html=../../code-coverage.out -o ../../code-coverage.html

lint: ## Lint the files
	@go get -u golang.org/x/lint/golint
	@cd cmd/$(PROJECT_NAME) &&  golint -set_exit_status $(go list ./... | grep -v /vendor/)

build: dep ## Build the binary file
	@mkdir -p bins
	@CGO_ENABLED=0 GOOS=linux go build -v -o ./bins/${FUNCTION_NAME} ./cmd/$(PROJECT_NAME)

clean: ## Remove manual build bin
	@rm -rf bins/ code-coverage.*

betaDeploy: clean build ##Deploy a local build to Beta environnment
	@cd bins;zip ${FUNCTION_NAME}.zip ${FUNCTION_NAME}
	@aws lambda update-function-code --region ${REGION} --function-name ${FUNCTION_NAME} --zip-file fileb://bins/${FUNCTION_NAME}.zip  --profile beta

releaseVersion: ## Make a version of existing Lambda with STAGING Alias
	@cd bins;zip ${FUNCTION_NAME}.zip ${FUNCTION_NAME}
	@aws lambda delete-alias --function-name ${FUNCTION_NAME} --name FALLBACK
	@aws lambda get-alias --function-name ${FUNCTION_NAME} --name LIVE \
	--output text --query 'FunctionVersion' | xargs -I {} aws lambda create-alias --function-name ${FUNCTION_NAME} --name FALLBACK --function-version {}
	@aws lambda get-alias --function-name ${FUNCTION_NAME} --name LIVE \
	--output text --query 'FunctionVersion' | xargs -I {} aws lambda update-alias --region ${REGION} --function-name ${FUNCTION_NAME} --name FALLBACK --function-version {}
	@aws lambda update-function-code --function-name ${FUNCTION_NAME} --zip-file fileb://bins/${FUNCTION_NAME}.zip --publish \
	--output text --query 'Version' | xargs -I {} aws lambda update-alias --region ${REGION} --function-name ${FUNCTION_NAME} --name LIVE --function-version {}

revert: ## Switch back to previous Live Lambda (requires ReleaseVersion to make an alias)
	@aws lambda get-alias --function-name ${FUNCTION_NAME} --name FALLBACK \
	--output text --query 'FunctionVersion' | xargs -I {} aws lambda update-alias --region ${REGION} --function-name ${FUNCTION_NAME} --name LIVE --function-version {}
	@aws lambda delete-alias --function-name ${FUNCTION_NAME} --name FALLBACK

help: ## Display this help screen
	@grep -h -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'
