# lambda.pipeline

A method of deploying Golang lambdas using GitLab CI to progressive environment (QA, Staging).

- CI - unit test and code quality 
- CD - Alias deployments

### Infrastructure

It is important to note that the following guide does not instruct the user to create the necessary infrastructure in AWS. 


#### Folder structure

```
lambda.example
  - cmd
    - main.go
    - app.go
    - app_test.go
  - Makefile
  - .gitignore
  - .gitlab-ci.yml
  - README.md
```

#### GitLab permissions

Each repository will be created in Gitlab with the following permissions:

- The master branch will be protected - cannot be pushed to directly.
- Author of MR cannot approve

### GitLab Runner

To allow for automation, a GitLab runner will be used which makes use of the .gitlab-ci.yml file. They will use the token available in CI/CD - Runner. 

#### Configuration
```
URL : https://gitlab.com/

token: <as below>

name lambda.<function name>

tags: docker

runtime: docker

image: alpine:latest
```

#### Make file

Makefile to be included in each template. The help file is displayed using make help. The GitLab pipeline will make use of the Makefile, however, the commands can be used manually using make <command>.

➜ make help
dep                            Get the dependencies
test                           Run unit tests
coverage                       Generate global code coverage report
lint                           Lint the files
build                          Build the binary file
clean                          Remove manual build bin
QADeploy                       Deploy a local build to QA environnment
releaseVersion                 Make a new live version of the Lambda and create a Fallback version
revert                         Switch back to previous Live Lambda (requires ReleaseVersion to make an alias)
help                           Display this help screen


#### .gitignore

This should be included in each Go Lambda function pipeline git repository. Caution should be made to not add compiled binaries to the git repository.

```
# Binaries for programs and plugins
*.exe
*.exe~
*.dll
*.so
*.dylib

# Test binary, built with `go test -c`
*.test

# Dependency directories
vendor/

# Compiled/zipped files for release should be omitted from VC
bins/*

# "go test"/"go tool cover" dump files
c.out
coverage.html
```

README.md

A minimum requirement for the read me file. The badges contain a gitlab repository path.

```
# <NAME>

###<Description>

[![Build Status](https://gitlab.com/<project>/<repo name>/badges/master/pipeline.svg)](https://gitlab.com/<repo>/<repo name>/commits/master) [![Coverage Report](https://gitlab.com/<repo>/<repo name>/badges/master/coverage.svg)](https://gitlab.com/<repo>/<project>/commits/master) [![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

```

#### .gitlab-ci.yml

YML definition of the stages of the pipeline. Automatically runs when conditions met. 


#### QA

To aid in the development of lambda functions in the AWS QA environment, the Makefile contains the following command. This will build the binary and then push it to the appropriate Lambda function in the AWS QA account. However, the automated pipeline will also incorporate the QA deployments as well.

make QADeploy

#### Merge Requests 

Opening a merge request to the master branch will invoke the Gitlab pipeline. In brief, this will build the binary and facilitate a manual deploy to the QA and Staging environments. The manual steps allow for the reviewing Developer / Tester to assess the function in each environment prior to promoting it to Production.

# Stages

## Pre-build

### Unit Test

The unit test will be run and an artifact created with the code coverage report. On completion of the Unit Test job, an HTML file is available as an artifact from the pipeline.

### Code Quality Report

This takes approximately 5 minutes to generate. This is a code report template provided by the Gitlab CI. The report is available during the MR for recommendations on code quality.

## Build

This will run automatically on an MR as the basis of deploying to each environment. 

## Deploy to QA / Staging

A manual job to prevent the unintentional deployment to the environments. This job can be triggered manually. Deploy to QA / Staging

## Revert

The revert job is an immediate roll back to the previous Lambda function in that environment. This is possible due to the use of an Alias created during the pipeline. 

It is possible to continue to deploy the Lambda function to Staging if required.

## Merge to Master

Once the Lambda function has been deployed to each environment, the MR can be approved and merged into master.

## Slack Monitoring

A webhook can be integrated with a Slack channel. The pipeline is accessible through 

set up must be completed in Slack under Incoming Webhooks. An example for the webhook URL woud be:

```
      curl -X POST -H 'Content-type: application/json' --data '{"text":"*'"$GITLAB_USER_NAME - $(cat .job_status) * |  *$CI_COMMIT_REF_NAME * into *$CI_MERGE_REQUEST_TARGET_BRANCH_NAME"'*\n```'"Commit Title: $CI_COMMIT_TITLE\n Pipeline: $CI_COMMIT_DESCRIPTION\n Merge Request: ${CI_MERGE_REQUEST_PROJECT_URL:-No Merge Request}\n Pipeline Details: $CI_PIPELINE_URL\n Job Detail :$CI_JOB_URL\n"'```\n>'"$CI_PROJECT_URL"'"}' https://hooks.slack.com/services/XXXXXXXX/XXXXXXX/XXXXXXXXXXXXXXXXXXX
```

* Please set $WEBHOOK_ENDPOINT in GitLab CI Variables

The links in the slack channel or via the repository in Gitlab. This will trigger on:

- A merge request being opened

- Deploy to QA / Staging

- Revert in QA / Staging